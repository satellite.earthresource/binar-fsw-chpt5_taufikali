// const for Player
const rock_div = document.getElementById("r1");
const paper_div = document.getElementById("p1");
const scissors_div = document.getElementById("s1");

const hideVersus = () => document.querySelector("b").style.display = 'none'
const result_div = document.querySelector(".result");
const disableInput = () => {
    document.querySelectorAll('.choice').forEach(input => {
        input.setAttribute('disabled', 'disabled')
    })

    document.querySelectorAll('.choice-hover').forEach(div => {
        div.classList.add('hover-stop');
    })
}

// const for COM
const rock_com = document.getElementById("r2");
const paper_com = document.getElementById("p2");
const scissors_com = document.getElementById("s2");

// Fungsi Computer Choice
function getComputerChoice() {
    const choices2 = ['r2', 'p2', 's2'];
    const randomNumber = Math.floor(Math.random() * 3);
    return choices2[randomNumber];
}

// result win lose and draw
function win(userChoice, computerChoice) {
    document.querySelector(".player-wins").style.display = 'block';
    document.querySelector(".vs").style.display = 'none';
    document.querySelector(".com-win").style.display = 'none';
    document.querySelector(".draw").style.display = 'none';
}

// function win() {
//     document.querySelector(".choices1").add(".chosen")
// } Gagal dalam penmabhan fungsi Add(element yg ada di CSS)


function lose(userChoice, computerChoice) {
    document.querySelector(".com-win").style.display = 'block';
    document.querySelector(".vs").style.display = 'none';
    document.querySelector(".player-wins").style.display = 'none';
    document.querySelector(".draw").style.display = 'none';
}


function draw(userChoice, computerChoice) {
    document.querySelector(".draw").style.display = 'block';
    document.querySelector(".vs").style.display = 'none';
    document.querySelector(".player-wins").style.display = 'none';
    document.querySelector(".com-win").style.display = 'none';
}

function disableClick() {
    document.querySelector(".choices1")
}
document.body.addEventListener("click", disableClick(), {
    once: true
})
// Coba disabled function---masi gagal, kondisi--setelah 2 kondisi terpenuhi baru berhenti
function disableInputPlayer(choosen) {
    document.getElementById(choosen).setAttribute('disabled', 'disabled')
}

//clear the compouter choice
function clearCompChoice() {
    document.getElementById("r2").classList.remove('chosen');
    document.getElementById("p2").classList.remove('chosen');
    document.getElementById("s2").classList.remove('chosen');
}


// Comparison win,lose,draw
function game(userChoice) {
    disableInput()
    const computerChoice = getComputerChoice();
    clearCompChoice();
    document.getElementById(computerChoice).classList.add("chosen");
    switch (userChoice + computerChoice) {
        case "r1s2":
        case "p1r2":
        case "s1p2":
            win(userChoice + computerChoice);
            break;
        case "r1p2":
        case "p1s2":
        case "s1r2":
            lose(userChoice + computerChoice);
            break;
        case "r1r2":
        case "p1p2":
        case "s1s2":
            draw(userChoice + computerChoice);
            break;
    }
}

// Player Pick
function main() {
    rock_div.addEventListener('click', function () {
        game("r1");
    })

    paper_div.addEventListener('click', function () {
        game("p1");
    })

    scissors_div.addEventListener('click', function () {
        game("s1");
    })
}

main();

function refresh() {
    clearCompChoice();
    document.querySelectorAll('.choice-hover').forEach(div => {
        div.classList.remove('hover-stop');
    })
    document.querySelector(".player-wins").style.display = 'none';
    document.querySelector(".vs").style.display = 'block';
    document.querySelector(".com-win").style.display = 'none';
    document.querySelector(".draw").style.display = 'none';

    document.getElementById('r1')
        .removeAttribute('disabled');
    document.getElementById('s1')
        .removeAttribute('disabled');
    document.getElementById('p1')
        .removeAttribute('disabled');
}