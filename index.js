const express = require('express')
const app = express()
const path = require('path')
const port = 5000

const users = require('./users.json')


app.use(express.static('public'))
app.use(express.static('files'))

app.use('/static', express.static('public'))

app.get('/', (req, res) => {
    res.send('Hello Lecuter, WELCOME TO MY CHAPTER 5 CHALLANGE PAGE!')
})
// Static File Chapter 3 & 4
app.get('/chapter3', (req, res) => {
    res.sendFile(path.join(__dirname + '/public/Chpt3/index.html'));
})

app.get('/chapter4', (req, res) => {
    res.sendFile(path.join(__dirname + '/public/Chpt4/index.html'));
})

// JSON file-ascending
app.get('/users', (req, res) => {
    function compare(a, b) {
        const nameA = a.name.toUpperCase();
        const nameB = b.name.toUpperCase();

        let comparison = 0;
        if (nameA > nameB) {
            comparison = 1;
        } else if (nameA < nameB) {
            comparison = -1;
        }
        return comparison;
    }
    res.json(users.users.sort(compare));
})




app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})